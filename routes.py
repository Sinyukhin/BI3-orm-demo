#Импорт из текущего пакета -- там у нас экземпляр приложения.
from . import app, db
from flask import url_for, render_template, render_template_string

from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, SubmitField
from wtforms.validators import DataRequired

@app.route('/')
def main_page():
#	return render_template("base.html")
	return render_template_string("""
	{% extends "base.html"%}
	{% block main %}"""
	"<ul>"
	f"<li><a href=\"{url_for('add_user')}\">Добавить пользователя</a></li>"
	f"<li><a href=\"{url_for('view_users')}\">Посмотреть пользователей</a></li>"
	"</ul>"
	"""{% endblock %}
	""")

class AddUserForm(FlaskForm):
	name = TextField('Имя', validators=[DataRequired()])
	password = PasswordField('Пароль', validators=[DataRequired()])
	submit = SubmitField("Создать")

from .models import User
@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
	_form = AddUserForm()
	if (_form.validate_on_submit()):
		u = User(name = _form.name.data, pass_hash = _form.password.data)
		db.session.add(u)
		db.session.commit()
	return render_template("add_user.html", form = _form)

@app.route('/view_users')
def view_users():
	_users = User.query.all()
	return render_template("view_users.html", users = _users)
	
