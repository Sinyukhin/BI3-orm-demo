from flask import Flask, render_template
from .config import DevConfig
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object(DevConfig)

#Создаём коннекшн к базе данных
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from .routes import *
from .models import *
